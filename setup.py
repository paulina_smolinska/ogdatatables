import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()


os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='ogdatatables',
    version='1.1.4',
    packages=find_packages(),
    include_package_data=True,
    license='all rights reserved',
    description='Django datatables app',
    long_description=README,
    install_requires=['six>=1.10'],
    url='ordergroup.pl',
    author='Order Group',
    author_email='hello@ordergroup.pl',
    ckassufuers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
