# Datatables 1.0.0
## Wymagania
- django-dajaxice-ng
- jquery >= 1.10.1 (możliwe że jakiś starszy zadziała)

## Instalacja

- Zainstaluj dajaxice-ng zgodnie z instrukcją [(link)](http://django-dajaxice-ng.readthedocs.io/installation.html)

- Wybierz i zainstaluj odpowiednią wersje ogdatatables poprzez pip:

```
# branch master:
pip install git+https://bitbucket.org/ordergroup/ogdatatables.git
# branch absolute-admin (wersja z szablonami dostosowanymi pod absolute-admin):
pip install git+https://bitbucket.org/ordergroup/ogdatatables.git@absolute-admin
```

- dodaj paczke ogdatatables do `INSTALLED_APPS`:

```
INSTALLED_APPS = [
    ...
    'ogdatatables',
    ...
]
```

- W swojej aplikacji utwórz plik tables.py i wypełnij go w następujący sposób:

```python
from ogdatatables.table import Datatable
from .models import twoj_model
from django.template import loader as template_loader


class TwojmodelListDatatable(Datatable):
    columns = (
        (u'nazwa kolumny', 'nazwa_pola_w_modelu'),
        (u'nazwa_kolejnej_kolumny', 'nazwa_kolejnego_pola')
        ...
    )
    queryset = twoj_model.objects.all()
    row_template = template_loader.get_template('sciezka_do_pliku_row/row.html')
```

- Dodaj do contextu w odpowiednim widoku następującą treść:

```
def get_context_data(self, **kwargs):
    ...
    context['nazwa_obiektu_z_contextu'] = TwojmodelListDatatable(self.request, ordering_column_index=0, **kwargs)
    ...
```

- Utwórz plik `row.html` w jednym z folderów z szablonami i wypełnij go przykładową treścią:

```html
<td><a href="">{{ element.title }}</a></td>
<td>{{ element.description }}</td>
```

- Dodaj następujące fragmenty do szablonu docelowego widoku:
    - na początku pliku:

```html
{% load tables %}
```

- w sekcji head należy dodać oryginalne css przewidzane dla datatables - jest to kluczowe żeby wszystko wyświetlało się poprawnie

- w sekcji head (ważne żeby plik .js znajdował się nad wywołaniem template taga z ostatniego kroku):

```html
<script src="{{STATIC_URL}}ogdatatables/datatables.js"></script>
<link href="{{STATIC_URL}}ogdatatables/datatables.css" rel="stylesheet" type="text/css">
```

- w miejscu w którym ma się pojawić tabela:

```
{% datatable nazwa_obiektu_z_contextu %}
```
## Zmiana submodułu na paczkę

W wypadku gdy projekt posiada już poprawnie zainstalowany submoduł datatables, do podmiany na paczkę należy wykonać następujące kroki:

- instalacja odpowiedniej wersji paczki
- zmiana rekordu w `INSTALLED_APPS` z `datatables` na `ogdatatables`
- zmiana wszystkich importów w projekcie z `datatables` na `ogdatatables`
- zmiana includów w html ze `{{ STATIC_URL }}datatables/` na `{{ STATIC_URL }}ogdatatables/`
- Po sprawdzeniu czy paczka poprawnie działa, [usunięcie submodułu z projektu](http://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule).