module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        coffee: {
            compile: {
                files: {
                    '../server/public/static/kyl/js/kyl.js': ['scripts/*.coffee']
                }
            }
        },
        compass: {
            datatables: {
                options: {
                    sassDir: 'styles/',
                    cssDir: '../ogdatatables/static/ogdatatables/'
                },
                compile: {
                    options: {
                        debugInfo: false
                    }
                }
            }
        },
        watch: {
            compass: {
                files: 'styles/{,*/}*.{scss,sass}',
                tasks: ['compass:datatables:compile']
            },
            coffee: {
                files: 'scripts/{,*/}*.coffee',
                tasks: ['coffee:compile']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['coffee', 'compass']);
};