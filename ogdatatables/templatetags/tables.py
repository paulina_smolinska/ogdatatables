from django import template
from six import text_type

register = template.Library()


@register.simple_tag()
def datatable(table):
    return table.render()


@register.filter
def has_selected(key, value):
    return key, value


@register.filter
def in_table(item, table):
    key, value = item
    value = text_type(value)
    if table.dropdown_filters_values:
        if text_type(table.dropdown_filters_values.get(key, None)) == value:
            return 'selected="selected"'
    return ''
