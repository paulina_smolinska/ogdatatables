"""Modul do tworzenia tabel.

Tabele w html'u sa tworzone uzywajac template tagu `table`, ktory jako argument przyjmuje
obiekt klasy Datatable.

W ten sposob stworzone tabele moga byc

- porzadkowane wg. wartosci w poszczegolnych kolumnach
- filtrowane / przeszukiwane
- paginowane

Uzywanie tabel wymaga dolaczenia do strony:

- tables.js
- dajaxice
- tables.css
- jQuery
- cssow (js nie jest potrzebny) dla [jQuery DataTables](http://www.datatables.net/)
    - chyba, ze ktos uzywa wlasnego template

Tabele tworzy sie subklasujac abstrakcyjna klase Datatable i ustawiajac jej
odpowiedni atrybuty, analogicznie do np. `template` w Class-based views.

Example:
    tables.py
        import datatables

        class MoviesTable(datatables.Datatable):
            queryset = Movie.objects.all()
            columns = ((u'Tytul', 'title'),
                       (u'Rezyser', 'director__name'),
                       )
            row_template = template_loader.get_template('tables/movie_row.html')
            filter_function = datatables.make_icontains_filter('title', 'director__name')
            title = u'filmy'
            css_class = 'table-info'

    view.py:
        from tables.py import MoviesTable

        class TableView(View):
            def get_context_data(self, **kwargs):
                context = super(TableView, self).get_context_data(**kwargs)
                context['movies_table'] = MoviesTable()
                return context

    template.html:
        {% load tables %}
        {% datatable movies_table %}
"""
from __future__ import absolute_import, division, print_function

import json
from uuid import uuid1

import six
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.aggregates import Count
from django.template import RequestContext, Context
from django.template.loader import render_to_string


class Datatable(object):
    """Abstrakcyjna klasa tabel.

    Class attributes:
        queryset (Queryset): dane ktore maja byc wyswietlone w tabli, *alternatywnie* zamiast tego moze zostac
            zdefiniowana metoda get_queryset()
        columns (list of (string, ordering)): lista tytulow kolumn wraz z metoda sortowania po danej kolumnie
            ordering moze to byc
            - string - wtedy jest traktowane jako nazwa pola i wywolywane jest order_by(ordering)
            - list of string - traktowane jako list nazw pol i wywloywane jest order_by(*ordering)
            - callable - funkcja sortujaca i wywolywane jest ordering(queryset, ascending)
                zeby uzyskac uporzadkowany queryset
        filter_function (callable, optional): funkcja ktore przyjmuje queryset i query w postaci stringa i
            zwraca przefiltrowany queryset
        row_template (Template)
        template (optional)
        title (optional)
        css_class (optional): przekazywane do template
        html_id (optional): gdy nie jest podany uzywane jest id wygenerowane przez uuid1
        default_ordering_column_index (optional)
        default_order_ascending (optional)
        on_page_options (optional)
        on_page_default (optional): gdy nie podane pierwsze wartosc z on_page_options jest brana
        dropdown_filters ([string, string, [value, string]]): lista krotek na podstawie ktorej tworzone sa
            filtry - [(tytul filtra, lookup pola, [(wartosc, nazwa wartosci)])]
        **kwargs: dodatkowe argumenty zostaja umieszone w dictcie self.kwargs a takze przekazane do
            contextu row_template, wszystkie takie argumenty musza byc serializowalne do jsona (uzywajac json.dumps)

    Opcjonalne metody do zdefiniowania:
        is_request_allowed(self, request) -> bool
        get_queryset(self) -> Queryset: wykorzystywana tylko gdy not self.queryset

    """

    queryset = None
    columns = None
    row_template = None
    filter_function = None
    advanced_filter_function = None

    _container_template = 'tables/datatable-container.html'
    rows_template = 'tables/datatable-rows.html'
    template = 'tables/datatable.html'
    title = ''
    css_class = ''
    html_id = None

    default_ordering_column_index = None  # column index
    default_order_ascending = True
    null_values_last = True
    on_page_options = (10, 50, 100)
    on_page_default = None

    def __init__(self, page=None, on_page=None, query=None,
                 ordering_column_index=None, ordering_ascending=True,
                 dropdown_filters_values=None,
                 request=None, include_container=True, null_values_last=True, **kwargs):
        super(Datatable, self).__init__()
        self.request = request
        self.kwargs = kwargs
        self.has_filter_function = bool(self.filter_function or self.advanced_filter_function)
        if dropdown_filters_values:
            self.dropdown_filters_values = {k: v for k, v in six.iteritems(dropdown_filters_values) if v}
        else:
            self.dropdown_filters_values = None
        self.html_id = self.html_id or str(uuid1())
        self.null_values_last = null_values_last
        self._include_container = include_container
        self.reload(page, on_page, query, ordering_column_index, ordering_ascending, dropdown_filters_values)

    @property
    def columns_safe(self):
        columns = []
        for column in self.columns:
            column = list(column)[:3]
            col_len = len(column)
            if col_len < 3:
                column = column + [''] * (3 - col_len)
            columns.append(column)
        return columns

    def is_request_allowed(self, request):
        return True

    def to_json(self):
        d = {
            'datatables__module__': self.__class__.__module__,
            'datatables__class__': self.__class__.__name__,
            '__html_id__': self.html_id,
            'kwargs': self.kwargs,
        }
        # print(json.dumps(d))
        return json.dumps(d)

    @staticmethod
    def from_json(json_string):
        parsed_json = json.loads(json_string)
        import importlib
        module = importlib.import_module(parsed_json['datatables__module__'])
        cls = getattr(module, parsed_json['datatables__class__'])
        cls.html_id = parsed_json['__html_id__']
        return cls, parsed_json['kwargs']

    def reload(self, page=None, on_page=None, query='',
               ordering_column_index=None, ordering_ascending=True,
               dropdown_filters_values=None):
        # prepare values
        self.page = page or 1
        on_page = on_page or self.on_page_default or self.on_page_options[0]
        self.on_page = int(on_page)
        self.query = query
        if ordering_ascending == 'False' or ordering_ascending == 'false':
            ordering_ascending = False
        self.ordering_ascending = ordering_ascending
        if ordering_column_index == 0:
            self.ordering_column_index = 0  # 0 jest dobra wartoscia, uznawanie jest za False psuje tu
        else:
            if not ordering_column_index and self.default_ordering_column_index is not None:
                self.ordering_column_index = self.default_ordering_column_index
                self.ordering_ascending = self.default_order_ascending
            else:
                self.ordering_column_index = ordering_column_index
                self.ordering_ascending = ordering_ascending
        data = self.get_queryset() if self.queryset is None else self.queryset
        data.table = self
        if query:
            if self.filter_function and self.advanced_filter_function:
                data = self.combine_querysets(data, self.advanced_filter_function(data, query),
                                              self.filter_function(data, query))
            elif self.filter_function:
                data = self.filter_function(data, query)
            elif self.advanced_filter_function:
                data = self.advanced_filter_function(data, query)
                # print(data.query)
        data = self._filter_by_dropdowns(data)
        # order
        self.data = self._order(data)
        # paginate
        self.paginator = self._paginate()
        # set page
        self.page_data = self._set_page()
        self._set_page_range()

    def combine_querysets(self, data, q1, q2):
        all_object_ids = list(set(list(q1.values_list('pk', flat=True)) + list(q2.values_list('pk', flat=True))))
        return data.filter(pk__in=all_object_ids)

    def _filter_by_dropdowns(self, data):
        if not self.dropdown_filters_values:
            return data
        return data.filter(**self.dropdown_filters_values)

    def _order(self, data):
        ordering_column_index = self.ordering_column_index
        if ordering_column_index is None or ordering_column_index == 'None':
            return data
        self.ordering_column_index = int(ordering_column_index)
        ordering = self.columns[self.ordering_column_index][1]
        if ordering is None:
            return data
        prefix = '' if self.ordering_ascending else '-'
        null_prefix = '' if not self.ordering_ascending else '-'
        if callable(ordering):
            result_data = ordering(data, ascending=self.ordering_ascending, request=self.request)
        elif isinstance(ordering, tuple) or isinstance(ordering, list):
            try:
                if self.null_values_last:
                    order_fields = []
                    order_fields_null = []
                    annotate_kwargs = {}
                    for order_field in ordering:
                        null_field = 'null_{}'.format(order_field)
                        annotate_kwargs[null_field] = Count(order_field)
                        order_fields_null.append(null_prefix + null_field)
                        order_fields.append(prefix + order_field)
                    data = data.annotate(**annotate_kwargs)
                    result_data = data.order_by(*(order_fields_null + order_fields))
                else:
                    result_data = data.order_by(*[prefix + o for o in ordering])
            except AttributeError:
                pass
        else:
            if self.null_values_last:
                data = data.annotate(null_field=Count(ordering, distinct=True))
                result_data = data.order_by(null_prefix + 'null_field', prefix + ordering)
            else:
                result_data = data.order_by(prefix + ordering)
        return result_data

    def _paginate(self):
        paginator = Paginator(self.data, self.on_page)
        return paginator

    def _set_page_range(self):
        # TODO: sparametryzowac to jakos
        num = self.paginator.num_pages
        if num <= 10:
            prange = range(1, num + 1)
        elif 6 <= self.page <= num - 5:
            prange = [1, '...']
            prange += list(range(max(2, self.page - 3), min(num, self.page + 4)))
            prange.append('...')
            prange.append(num)
        elif self.page < 6:
            prange = range(1, self.page + 4)
            prange.append('...')
            prange.append(num)
        else:
            prange = [1, '...']
            prange += list(range(self.page - 3, num + 1))
        self.paginator.pagerange = prange

    def _set_page(self):
        try:
            page_data = self.paginator.page(self.page)
        except PageNotAnInteger:
            self.page = 1
            page_data = self.paginator.page(1)
        except EmptyPage:
            self.page = self.paginator.num_pages
            page_data = self.paginator.page(self.paginator.num_pages)
        return page_data

    def render(self, addtional_args=None):
        if not self.is_request_allowed(self.request):
            return ''
        if addtional_args:
            addtional_args['table'] = self
            rendered_table = render_to_string(self.template, addtional_args)
        else:
            rendered_table = render_to_string(self.template, {'table': self})
        if not self._include_container:
            return rendered_table
        return render_to_string(self._container_template, {'table': self, 'rendered': rendered_table})

    def rendered_rows(self):
        result = []
        for element in self.page_data:
            self.kwargs['element'] = element
            if self.request:
                try:
                    rendered_row = self.row_template.render(RequestContext(self.request, self.kwargs))
                except TypeError:
                    rendered_row = self.row_template.render(context=self.kwargs, request=self.request)
            else:
                try:
                    rendered_row = self.row_template.render(Context(self.kwargs))
                except TypeError:
                    rendered_row = self.row_template.render(context=self.kwargs)
            result.append(rendered_row)
        if 'element' in self.kwargs:
            del self.kwargs['element']
        return result

    def rows(self):
        return render_to_string(self.rows_template, {'table': self})

    def get_dropdown_filters(self):
        dropdown_filters = getattr(self, 'dropdown_filters', None)
        if not dropdown_filters:
            return
        output_filters = []
        for filter_name, filter_field, values in dropdown_filters:
            if isinstance(values, six.string_types):
                values_attr = getattr(self, values, None)
                if callable(values_attr):
                    values = values_attr()
                else:
                    values = values_attr
            if values:
                output_filters.append((filter_name, filter_field, values))
        return output_filters
