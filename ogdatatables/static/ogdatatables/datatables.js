var ajaxDatatables = (function () {
	function getOnPage(table){
		return table.find('[name=table_size]').val();
	}

	function getQuery(table){
		return table.find('[name=search]').val();
	}

	function getOrdering(table){
		return table.data('ordering');
	}

	function getOrderingAscending(table){
		return table.data('ordering-ascending')
	}

	function getDropdownFilters(table){
		var selects = table.find('.table-filter-select');
		var result = {};
		selects.each(function(){
			$this = $(this);
			result[$this.attr('name')] = $this.val();
		});
		console.log(result);
		return result;
	}

	function setLoader(table){
		table.find('.dataTables_info').text('Ładuje...');
		return setTimeout(function() {
			var body = table.find('tbody');
			var height = Math.max(body.height() - 1, 184);
			var width = body.width();
			var loader = $('<div id="loader" class="loader" style="margin: 0;">Ładuje...</div>');
			var loaderContainer = $('<div class="load2 table-loader-l">');
			loaderContainer.html(loader);
			loaderContainer.height(height + 'px');

			var cell = $('<td style="padding: 0;">');
			cell.html(loaderContainer);
			var thead = table.find('thead > tr');
			thead.find('th').each(function (index, th){
				var th = $(th);
				var thWidth = th.width();
				th.width(thWidth);
			});
			var colsNumber = thead.children().length;
			cell.attr('colspan', colsNumber);

			var row = $('<tr>');
			row.html(cell);
			body.html(row);

			loader.css('position: relative;');
			loader.css('top', (height / 2 - loader.height() / 2) + 'px');
			loader.css('left', (width / 2 - loader.width() / 2) + 'px');

			var bg = cell.css('background-color');
			function changeAfterBefore (selector, property, argument) {
				$('<style>' + selector + '{' + property + ': ' + argument + ' !important;}</style>').appendTo('head');
			}
			changeAfterBefore('.load2 .loader:before', 'background', bg);
			changeAfterBefore('.load2 .loader:after', 'background', bg);
		}, 500);
	}

	function setTable(table, json){
		function callback (timeout){
			return function (data) {
				clearTimeout(timeout);
				var id = table.attr('id');
				var parent = table.parent();
				parent.html(data);
				setTable($('#' + id), json);
			}
		}
		function reloadTable(table, page){
			timeout = setLoader(table);
			Dajaxice.ogdatatables.reload_datatable(
				callback(timeout),
				args={
					json: json,
					page: page,
					on_page: getOnPage(table),
					query: getQuery(table),
					ordering: getOrdering(table),
					ordering_ascending: getOrderingAscending(table),
					dropdown_filters: getDropdownFilters(table),
				}
			)
		}

		table.bind('reload', function(){
			reloadTable($(this),  $(this).data('current-page'));
		});

		table.find('.page-select-btn').click(function (e){
			e.preventDefault();
			reloadTable(table, $(this).data('page-index'));
		});
		table.find('[name=table_size]').change(function (e){
			reloadTable(table);
		});
		table.find('.table-filter-select').change(function (e){
			reloadTable(table);
		});
		table.find('[name=search]').keypress(function (e){
			if (e.which == 13){
				reloadTable(table);
			}
		});
		var sortingBtns = table.find('.sorting');
		sortingBtns.click(function (e){
			$this = $(this);
			var ascending = $this.data('ordering-ascending');
			sortingBtns.removeClass('sorting_asc');
			sortingBtns.removeClass('sorting_desc');
			sortingBtns.addClass('sorting');
			if (ascending == 'True'){
				ascending = false;
				suffix = '_desc';
			} else {
				ascending = true;
				suffix = '_asc'
			}
			$this.data('ordering-ascending', ascending);
			table.data('ordering-ascending', ascending);
			var ordering = $this.data('ordering');
			table.data('ordering', ordering);
			$this.addClass('sorting' + suffix);
			reloadTable(table);
		});
	}

	return {
		'setTable': setTable,
	}
})();
