# -*- coding: utf-8 -*-
"""Modul zawiera funkcje filtrujace querysety.

Funkcje te w wiekszosci przypadkow powinny wykorzystane w definicji
nowej funkcje filtrujacej, nie zamiast jej, poniewaz maja nieodpowiednie
sygnatury.

"""
from __future__ import absolute_import, division, print_function
from django.db.utils import ProgrammingError


def icontains_filter(data, query, *args):
    """Filtruje tabele.

    Uzywa `icontains` lookup na kazdej nazwie pola z args, robiac na nich `or`.

    Args:
        data (queryset): queryset do przefiltrowania
        query (string)
        args ([string]): nazwy pol ktore maja byc uwzglednione w filtrowaniu

    Returns:
        przefiltrowany queryset
    """
    from django.db.models import Q
    q = Q()
    for field_name in args:
        q |= Q(**{field_name + '__icontains': query})
    try:
        return data.filter(q)
    except AttributeError:
        return data


def icontains_filter_advanced(data, query, *args):
    """Filtruje tabele.

    Buduje ręcznie query "WHERE" w sql filtrujące bezpośrednio na czystym sql. Niestety nie używa lookup'ów,
    więc trzeba ręcznie definiować strukturę joinów. Wszystkie filtry są łączone OR-em.

    Args:
        data (queryset): queryset do przefiltrowania
        query (string)
        args ([string]/iterable[string/(klasa, nazwa_pola_rel))): nazwy pol ktore maja byc uwzglednione w filtrowaniu
            Warianty:
                - string np "field" - zwykłe filtrowanie po jednym polu - UWAGA, może nie działać przy spanie przez
                    zewnętrzna tabelę, np po auth_user - TODO - do sprawdzenia/poprawy
                - krotka - (nazwa tabeli, nazwa pola docelowego do joina, nazwa pola bazowego do joina, [lista pól w
                    tabeli docelowej, które mają być zsumowane]),
                    przykład::  ('auth_user', 'id', 'client_id', ['first_name', 'last_name'])

    Returns:
        przefiltrowany queryset
    """
    where_clauses = []
    alias_counter = 1
    for field_spec in args:
        target_table, target_field, source_field, concatenated_fields = field_spec
        if type(concatenated_fields) in [tuple, list]:
            field_sql = u" || ' ' || ".join(concatenated_fields)
        else:
            field_sql = field_spec
        where_clauses.append(u"""EXISTS (SELECT * FROM {0} as C{1} WHERE {2}=C{1}.{3} AND UPPER({4}) LIKE UPPER(%s))""".format(
            target_table, alias_counter, source_field, target_field, field_sql
        ))
        alias_counter += 1
    where_clause = u" OR ".join(where_clauses)
    where_params = [u'%{}%'.format(query)] * len(args)
    try:
        data = data.extra(where=[where_clause], params=where_params)
        filtered_data = data.extra(where=[where_clause], params=where_params)
        return filtered_data
    except ProgrammingError:
        return data


def make_icontains_filter(*args):
    """Tworzy funkcje filtrujaca.

    Args:
        *args: nazwy pol po ktorych ma filtrowac (uzywajac __icontains)
    """

    def f(self, data, query):
        return icontains_filter(data, query, *args)

    return f


def make_icontains_filter_advanced(*args):
    """Tworzy funkcje filtrujaca w wariancie rozbudowanym
        Wersja ta pozwala na przeszukiwanie w polach relatywnych i ich konkatenacjach. Specyfikacja parametrów

    Args:
        *args: nazwy pol po ktorych ma filtrowac lub krotki, struktura opisana w docstringu icontains_filter_advanced.
    """

    def f(self, data, query):
        return icontains_filter_advanced(data, query, *args)

    return f


def boolean_filter_options():
    return [(1, 'Tak'), (0, 'Nie')]
